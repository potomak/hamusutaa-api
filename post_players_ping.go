package hamusutaa

import (
	"fmt"
	"net/http"
	"time"

	"google.golang.org/appengine"
	"google.golang.org/appengine/datastore"
	"google.golang.org/appengine/log"
	"google.golang.org/appengine/urlfetch"
)

// POST /players/:id/ping
// Creates a `ping` record between the current player and the selected player.
func handlePOSTPlayersPingWithId(playerId int64, player Player, token Token, currentPlayer Player, w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
	log.Debugf(c, fmt.Sprintf("Ping player id: %d", playerId))

	// Return an error if the selected player is active, only non-active players
	// can be pinged
	if player.IsMoving {
		http.Error(w, "You can't ping a player that is active", http.StatusBadRequest)
		return
	}

	// Return an error if the selected player is not owned by the current player
	currentPlayerKey := datastore.NewKey(c, "Player", "", token.PlayerId, nil)
	q := datastore.NewQuery("Transaction").Ancestor(currentPlayerKey).Filter("PlayerId =", playerId)
	transactionsCount, err := q.Count(c)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	log.Debugf(c, fmt.Sprintf("Found %d transactions", transactionsCount))
	if transactionsCount < 1 {
		http.Error(w, "You can't ping a player you don't own", http.StatusBadRequest)
		return
	}

	// Return an error if the selected player has already been pinged
	playerKey := datastore.NewKey(c, "Player", "", playerId, nil)
	// Note: the filter should be redundant because in the previous validation
	// we already checked for a non-active user, meaning that the last
	// should have `IsMoving == false`.
	measurementsQuery := datastore.NewQuery("Measurement").Ancestor(playerKey).Filter("IsMoving =", false).Order("-CreatedAt").Limit(1)
	measurements := make([]Measurement, 0, 1)
	if _, err := measurementsQuery.GetAll(c, &measurements); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	log.Debugf(c, fmt.Sprintf("Last measurement %#v", measurements))
	q = datastore.NewQuery("Ping").Ancestor(playerKey)
	if len(measurements) > 0 {
		q = q.Filter("CreatedAt >=", measurements[0].CreatedAt)
	}
	pingsCount, err := q.Count(c)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	log.Debugf(c, fmt.Sprintf("Pings from last measurement: %d", pingsCount))
	if pingsCount > 0 {
		http.Error(w, "You can't ping a player more than once", http.StatusBadRequest)
		return
	}

	// Create a new ping
	ping := &Ping{
		PlayerId:     token.PlayerId,
		BonusAwarded: false,
		CreatedAt:    time.Now(),
		UpdatedAt:    time.Now(),
	}
	log.Debugf(c, fmt.Sprintf("New ping: %#v", ping))

	pingKey := datastore.NewIncompleteKey(c, "Ping", playerKey)
	_, err = datastore.Put(c, pingKey, ping)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Create message
	data := map[string]interface{}{"event": PingEvent, "playerId": token.PlayerId, "name": currentPlayer.Name}
	msg := NewMessage(data, fmt.Sprintf("/topics/player-%d", playerId))

	// Create a Sender to send the message
	client := urlfetch.Client(c)
	sender := &Sender{ApiKey: ApiKey, Http: client}

	go func() {
		// Send the message and receive the response
		response, err := sender.SendNoRetry(msg)
		if err != nil {
			log.Errorf(c, fmt.Sprintf("Message delivery error: %q", err.Error()))
			return
		}
		log.Debugf(c, fmt.Sprintf("Message delivery response: %#v", response))
	}()

	w.WriteHeader(http.StatusNoContent)
}
