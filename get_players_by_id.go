package hamusutaa

import (
	"encoding/json"
	"fmt"
	"net/http"

	"google.golang.org/appengine"
	"google.golang.org/appengine/log"
)

// GET /players/:id
// Returns a JSON representation of the selected player.
func handleGETPlayersWithId(playerId int64, player Player, w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
	log.Debugf(c, fmt.Sprintf("Player id: %d", playerId))

	playerResponse := &PlayerResponse{
		Id:         playerId,
		Name:       player.Name,
		Color:      player.Color,
		IsMoving:   player.IsMoving,
		TotalSteps: player.TotalSteps,
		DailySteps: player.DailySteps,
		Price:      player.Price,
		Timezone:   player.Timezone,
		CreatedAt:  player.CreatedAt,
		UpdatedAt:  player.UpdatedAt,
	}

	js, err := json.Marshal(playerResponse)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(js)
}
