package hamusutaa

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"golang.org/x/net/context"

	"google.golang.org/appengine/datastore"
	"google.golang.org/appengine/log"
)

// GET /players/
// Returns a JSON representation of the list of all players.
func handleGETPlayers(c context.Context, w http.ResponseWriter, r *http.Request) {
	log.Debugf(c, "Players root")
	log.Debugf(c, fmt.Sprintf("URL.Query: %#v", r.URL.Query()))

	// TODO: handle pagination

	q := datastore.NewQuery("Player").Order("Price")
	s := r.URL.Query()["maxPrice"]
	if len(s) > 0 {
		m, err := strconv.Atoi(s[0])
		if err != nil {
			log.Errorf(c, fmt.Sprintf("Error parsing 'maxPrice': %q", err.Error()))
		} else {
			log.Debugf(c, fmt.Sprintf("Price <= %d", m))
			q = q.Filter("Price <=", m)
		}
	}
	var players []Player
	playerKeys, err := q.GetAll(c, &players)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	playersResponses := make([]*PlayerResponse, 0, len(players))
	for i, player := range players {
		playerResponse := &PlayerResponse{
			Id:         playerKeys[i].IntID(),
			Name:       player.Name,
			Color:      player.Color,
			IsMoving:   player.IsMoving,
			TotalSteps: player.TotalSteps,
			DailySteps: player.DailySteps,
			Price:      player.Price,
			Timezone:   player.Timezone,
			CreatedAt:  player.CreatedAt,
			UpdatedAt:  player.UpdatedAt,
		}
		playersResponses = append(playersResponses, playerResponse)
	}

	js, err := json.Marshal(playersResponses)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(js)
}
