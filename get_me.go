package hamusutaa

import (
	"encoding/json"
	"fmt"
	"net/http"

	"google.golang.org/appengine"
	"google.golang.org/appengine/datastore"
	"google.golang.org/appengine/log"
)

// GET /me
func handleGETMe(_ int64, _ Player, token Token, currentPlayer Player, w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
	log.Debugf(c, fmt.Sprintf("Me: %d", token.PlayerId))

	currentPlayerKey := datastore.NewKey(c, "Player", "", token.PlayerId, nil)
	q := datastore.NewQuery("Transaction").Ancestor(currentPlayerKey)
	var transactions []Transaction
	if _, err := q.GetAll(c, &transactions); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	log.Debugf(c, fmt.Sprintf("Found transactions: %#v", transactions))

	playerKeys := make([]*datastore.Key, 0, len(transactions))
	for _, transaction := range transactions {
		playerKeys = append(playerKeys, datastore.NewKey(c, "Player", "", transaction.PlayerId, nil))
	}

	players := make([]*Player, len(transactions))
	if err := datastore.GetMulti(c, playerKeys, players); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	log.Debugf(c, fmt.Sprintf("Found players: %#v", players))

	playersResponses := make([]*PlayerResponse, 0, len(players))
	for i, player := range players {
		playerResponse := &PlayerResponse{
			Id:         playerKeys[i].IntID(),
			Name:       player.Name,
			Color:      player.Color,
			IsMoving:   player.IsMoving,
			TotalSteps: player.TotalSteps,
			DailySteps: player.DailySteps,
			Price:      player.Price,
			Timezone:   player.Timezone,
			CreatedAt:  player.CreatedAt,
			UpdatedAt:  player.UpdatedAt,
		}
		playersResponses = append(playersResponses, playerResponse)
	}

	currentPlayerResponse := &CurrentPlayerResponse{
		Id:         token.PlayerId,
		Name:       currentPlayer.Name,
		Color:      currentPlayer.Color,
		IsMoving:   currentPlayer.IsMoving,
		TotalSteps: currentPlayer.TotalSteps,
		DailySteps: currentPlayer.DailySteps,
		Price:      currentPlayer.Price,
		Balance:    currentPlayer.Balance,
		Timezone:   currentPlayer.Timezone,
		CreatedAt:  currentPlayer.CreatedAt,
		UpdatedAt:  currentPlayer.UpdatedAt,
		Players:    playersResponses,
	}

	js, err := json.Marshal(currentPlayerResponse)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(js)
}
