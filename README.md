# hashire-hamusutaa

To run the app locally

    goapp serve -host 0.0.0.0

To deploy the app

    goapp deploy -application hashire-hamusutaa app.yaml

To deploy the datastore configuration

    gcloud preview app deploy index.yaml

To deploy the cron configuration

    gcloud preview app deploy cron.yaml

To run app test suite

    ./test.rb

## API

### GET /players/

Returns a JSON representation of the list of all players.

Note: it accepts a `maxPrice` (integer) query param to filter the list of
players by `price >= maxPrice`.

#### Request

    curl -v -X GET \
      http://localhost:8080/players/

#### Response

    200 OK

    [
      {
        "id": "5681726336532480",
        "name": "Giovanni",
        "color": "#00ccff",
        "isMoving": true,
        "totalSteps": 149,
        "dailySteps": 149,
        "price": 100,
        "timezone": "America/New_York",
        "createdAt": "2016-04-08T21:32:55.593679Z",
        "updatedAt": "2016-04-08T21:50:58.870806Z"
      },
      {
        "id": "6446574112604160",
        "name": "Giorgia",
        "color": "#00ccff",
        "isMoving": false,
        "totalSteps": 0,
        "dailySteps": 0,
        "price": 100,
        "timezone": "America/New_York",
        "createdAt": "2016-04-08T13:49:32.706893Z",
        "updatedAt": "2016-04-08T13:49:32.706893Z"
      }
    ]

### GET /players/:id

Returns a JSON representation of the selected player.

#### Request

    curl -v -X GET \
      http://localhost:8080/players/5681726336532480

#### Response

    200 OK

    {
      "id": "5681726336532480",
      "name": "Giovanni",
      "color": "#00ccff",
      "isMoving": false,
      "totalSteps": 0,
      "dailySteps": 0,
      "price": 100,
      "timezone": "America/New_York",
      "createdAt": "2016-04-05T17:53:19.742269Z",
      "updatedAt": "2016-04-05T17:53:19.742269Z"
    }

### GET /me

Returns a JSON representation of the authenticated player.

Notes:

* it needs an auth token

#### Request

    curl -v -X GET -H 'X-Auth-Token: 5118776383111168' \
      http://localhost:8080/me

#### Response

    200 OK

    {
      "id": "5681726336532480",
      "name": "Giovanni",
      "color": "#00ccff",
      "isMoving": false,
      "totalSteps": 0,
      "dailySteps": 0,
      "price": 100,
      "balance": 0,
      "timezone": "America/New_York",
      "players": [
        {
          "id": "6446574112604160",
          "name": "Giorgia",
          "color": "#00ccff",
          "isMoving": false,
          "totalSteps": 0,
          "dailySteps": 0,
          "price": 100,
          "createdAt": "2016-04-08T13:49:32.706893Z",
          "updatedAt": "2016-04-08T13:49:32.706893Z"
        }
      ],
      "createdAt": "2016-04-05T17:53:19.742269Z",
      "updatedAt": "2016-04-05T17:53:19.742269Z"
    }

### POST /players/

Creates a new player.

#### Request

    curl -v -X POST -H 'Content-Type: application/json' \
      -d '{"name": "Giovanni", "color": "#00ccff", "timezone": "America/New_York"}' \
      http://localhost:8080/players/

#### Response

    200 OK

    {
      "token": "5118776383111168",
      "playerId": "5681726336532480"
    }

### PUT /me

Updates the current player.

Notes:

* it needs an auth token

#### Request

    curl -v -X PUT -H 'Content-Type: application/json' \
      -H 'X-Auth-Token: 5118776383111168' \
      -d '{"name": "Giovanni", "color": "#ffffff", "timezone": "America/New_York"}' \
      http://localhost:8080/me

#### Response

    204 No Content

### POST /measurements

Creates a new `measurement` record associated to the current player.

New measurements update the current player record:

* current player's `totalSteps` will increase by `steps`
* current player's `balance` will increase by `steps`
* current player's `isMoving` will be set to `isMoving`

If the current player is moving in response to a ping from another player, that
is the last ping has been created less than `PingTTL` ago, the server will send
a `bonus` update to the player who sent the ping and will increase its balance
by `PingBonus`.

Notes:

* it needs an auth token
* it generates a `measurement` event on the current player's topic
* it could generate a `bonus` event on the channel of the player who pinged the
  current player

#### Event

    {
      "event": "com.yeahright.hamasutaa.MEASUREMENT",
      "isMoving": true
    }

#### Request

    curl -v -X POST -H 'Content-Type: application/json' \
      -H 'X-Auth-Token: 5118776383111168' \
      -d '{"steps": 12, "isMoving": true}' \
      http://localhost:8080/measurements

#### Response

    204 No Content

### POST /players/:id/buy

Creates a `buy` record between the current player and the selected player.

New transactions update the current player record:

* `balance` will be decreased by the selected player's `price`

New transactions update the selected player record:

* `price` will be increased by `DailyStepsGoal` $

Notes:

* it needs an auth token
* it is allowed only if the token isn't associated to the selected player (the
  player can't *buy* itself)
* the current player should have sufficient funds
* the current player shouldn't own the selected player already

#### Request

    curl -v -X POST -H 'X-Auth-Token: 5118776383111168' \
      http://localhost:8080/players/5681726336532480/buy

#### Response

    204 No Content

### POST /players/:id/ping

Creates a `ping` record between the current player and the selected player.

Notes:

* it needs an auth token
* it is allowed only if the token isn't associated to the selected player (the
  player can't *ping* itself)
* the selected player should be non-active
* the selected player should hasn't been pinged from its last measurement where
  `isMoving` is false
* it generates an event on the selected player's topic

#### Event

    {
      "event": "com.yeahright.hamasutaa.PING",
      "playerId": "5681726336532480",
      "name": "Giovanni"
    }

#### Request

    curl -v -X POST -H 'X-Auth-Token: 5118776383111168' \
      http://localhost:8080/players/5681726336532480/ping

#### Response

    204 No Content

### GET /tasks/daily

Resets the number of daily steps for players in timezones where is midnight and
computes bonuses for owners of players who reached the daily goal.

Notes:

* it's a cron task
* it needs auth as admin
* it generates several events on *players who will receive a bonus*' topics

#### Event

    {
      "event": "com.yeahright.hamasutaa.BONUS",
      "bonusAmount": 100
    }

#### Request

    curl -v -X GET \
      http://localhost:8080/tasks/daily

#### Response

    204 No Content
