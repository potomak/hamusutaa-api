package hamusutaa

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"google.golang.org/appengine"
	"google.golang.org/appengine/datastore"
	"google.golang.org/appengine/log"
)

// PUT /me
// Updates the current player.
func handlePUTMe(_ int64, _ Player, token Token, currentPlayer Player, w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
	log.Debugf(c, fmt.Sprintf("PUT player id: %d", token.PlayerId))

	decoder := json.NewDecoder(r.Body)
	var playerRequest PlayerRequest
	err := decoder.Decode(&playerRequest)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	log.Debugf(c, fmt.Sprintf("Update player request: %#v", playerRequest))

	updatedPlayer := &Player{
		Name:       playerRequest.Name,
		Color:      playerRequest.Color,
		IsMoving:   currentPlayer.IsMoving,
		TotalSteps: currentPlayer.TotalSteps,
		DailySteps: currentPlayer.DailySteps,
		Price:      currentPlayer.Price,
		Balance:    currentPlayer.Balance,
		Timezone:   playerRequest.Timezone,
		CreatedAt:  currentPlayer.CreatedAt,
		UpdatedAt:  time.Now(),
	}
	log.Debugf(c, fmt.Sprintf("Updated player: %#v", updatedPlayer))

	playerKey := datastore.NewKey(c, "Player", "", token.PlayerId, nil)
	_, err = datastore.Put(c, playerKey, updatedPlayer)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusNoContent)
}
