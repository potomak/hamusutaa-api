package hamusutaa

import (
	"fmt"
	"net/http"
	"strconv"
	"strings"

	"golang.org/x/net/context"

	"google.golang.org/appengine"
	"google.golang.org/appengine/datastore"
	"google.golang.org/appengine/log"

	"github.com/mjibson/appstats"
)

func init() {
	http.Handle("/players/", appstats.NewHandler(handlePlayers))
	http.Handle("/me", appstats.NewHandler(handleMe))
	http.Handle("/measurements", appstats.NewHandler(handleMeasurements))
	http.Handle("/tasks/", appstats.NewHandler(handleTasks))
}

func getURLSegments(prefix string, r *http.Request) []string {
	return strings.Split(r.URL.Path[len(prefix):], "/")
}

// Middleware that runs `next` only if an auth token exists.
func authToken(next AuthHandlerFunc) PlayerHandlerFunc {
	return func(playerId int64, player Player, w http.ResponseWriter, r *http.Request) {
		c := appengine.NewContext(r)
		tokenString := r.Header.Get("X-Auth-Token")
		log.Debugf(c, fmt.Sprintf("AuthToken middleware, token: %q", tokenString))

		if tokenString == "" {
			http.Error(w, "You need a valid token", http.StatusUnauthorized)
			return
		}

		tokenId, err := strconv.ParseInt(tokenString, 10, 64)
		if err != nil {
			http.Error(w, "You need a valid token", http.StatusUnauthorized)
			return
		}

		var token Token
		tokenKey := datastore.NewKey(c, "Token", "", tokenId, nil)
		err = datastore.Get(c, tokenKey, &token)
		if err == datastore.ErrNoSuchEntity {
			http.Error(w, "You need a valid token", http.StatusUnauthorized)
			return
		}
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		var currentPlayer Player
		currentPlayerKey := datastore.NewKey(c, "Player", "", token.PlayerId, nil)
		err = datastore.Get(c, currentPlayerKey, &currentPlayer)
		if err == datastore.ErrNoSuchEntity {
			http.Error(w, "You need a valid token", http.StatusUnauthorized)
			return
		}
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		log.Debugf(c, fmt.Sprintf("AuthToken middleware finish", tokenString))
		next.ServeHTTP(playerId, player, token, currentPlayer, w, r)
	}
}

// Middleware that runs `next` only if the token isn't associated to the
// selected player.
func forbidSamePlayer(next AuthHandlerFunc) AuthHandlerFunc {
	return func(playerId int64, player Player, token Token, currentPlayer Player, w http.ResponseWriter, r *http.Request) {
		c := appengine.NewContext(r)
		log.Debugf(c, fmt.Sprintf("ForbidSamePlayer middleware, token: %#v", token))

		if token.PlayerId == playerId {
			http.Error(w, "You're not allowed", http.StatusForbidden)
			return
		}

		log.Debugf(c, fmt.Sprintf("ForbidSamePlayer middleware finish"))
		next.ServeHTTP(playerId, player, token, currentPlayer, w, r)
	}
}

// Find player middleware
func findPlayer(next PlayerHandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		c := appengine.NewContext(r)
		segments := getURLSegments("/players/", r)
		log.Debugf(c, fmt.Sprintf("Segments: %q", segments))

		if segments[0] == "" {
			http.Error(w, "Player not found", http.StatusNotFound)
			return
		}

		playerId, err := strconv.ParseInt(segments[0], 10, 64)
		if err != nil {
			http.Error(w, "Player not found", http.StatusNotFound)
			return
		}

		var player Player
		playerKey := datastore.NewKey(c, "Player", "", playerId, nil)
		err = datastore.Get(c, playerKey, &player)
		if err == datastore.ErrNoSuchEntity {
			http.Error(w, "Player not found", http.StatusNotFound)
			return
		}
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		next.ServeHTTP(playerId, player, w, r)
	}
}

// Players resources router
func handlePlayers(c context.Context, w http.ResponseWriter, r *http.Request) {
	segments := getURLSegments("/players/", r)
	log.Debugf(c, fmt.Sprintf("Segments: %q", segments))

	switch r.Method {
	case "GET":
		if segments[0] == "" {
			handleGETPlayers(c, w, r)
			return
		}

		findPlayer(handleGETPlayersWithId)(w, r)
	case "POST":
		if segments[0] == "" {
			handlePOSTPlayers(c, w, r)
			return
		}

		switch segments[1] {
		case "ping":
			findPlayer(authToken(forbidSamePlayer(handlePOSTPlayersPingWithId)))(w, r)
		case "buy":
			findPlayer(authToken(forbidSamePlayer(handlePOSTPlayersBuyWithId)))(w, r)
		default:
			http.Error(w, "Not found", http.StatusNotFound)
		}
	default:
		w.Header().Set("Allow", "GET, POST")
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
	}
}

// Me resource router
func handleMe(c context.Context, w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET":
		authToken(handleGETMe)(-1, Player{}, w, r)
	case "PUT":
		authToken(handlePUTMe)(-1, Player{}, w, r)
	default:
		w.Header().Set("Allow", "GET, PUT")
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
	}
}

// Measurements resources router
func handleMeasurements(c context.Context, w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "POST":
		authToken(handlePOSTMeasurements)(-1, Player{}, w, r)
	default:
		w.Header().Set("Allow", "POST")
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
	}
}

// Tasks resources router
func handleTasks(c context.Context, w http.ResponseWriter, r *http.Request) {
	segments := getURLSegments("/tasks/", r)
	log.Debugf(c, fmt.Sprintf("Segments: %q", segments))

	switch r.Method {
	case "GET":
		switch segments[0] {
		case "daily":
			handleGETTasksDaily(c, w, r)
		default:
			http.Error(w, "Task not found", http.StatusNotFound)
		}
	default:
		w.Header().Set("Allow", "GET")
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
	}
}
