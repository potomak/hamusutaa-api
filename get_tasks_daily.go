package hamusutaa

import (
	"fmt"
	"net/http"
	"time"

	"golang.org/x/net/context"

	"google.golang.org/appengine/datastore"
	"google.golang.org/appengine/log"
	"google.golang.org/appengine/urlfetch"
)

const Midnight = 0

// GET /tasks/daily
// Resets the number of daily steps for players in timezones where is midnight
// and computes bonuses for owners of players who reached the daily goal.
func handleGETTasksDaily(c context.Context, w http.ResponseWriter, r *http.Request) {
	// Find distinct timezones
	timezonesQuery := datastore.NewQuery("Player").Project("Timezone").Distinct()
	var playerTimezones []Player
	if _, err := timezonesQuery.GetAll(c, &playerTimezones); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	log.Debugf(c, fmt.Sprintf("Found %d timezones", len(playerTimezones)))

	now := time.Now()
	log.Debugf(c, fmt.Sprintf("Time now: %q", now))

	// Find timezones where now is midnight
	var midnightTimezones []string
	for _, player := range playerTimezones {
		log.Debugf(c, fmt.Sprintf("Timezone: %q", player.Timezone))
		location, err := time.LoadLocation(player.Timezone)
		if err != nil {
			log.Errorf(c, fmt.Sprintf("Error %q trying to parse timezone: %q", err.Error(), player.Timezone))
			continue
		}
		nowLocal := now.In(location)
		log.Debugf(c, fmt.Sprintf("Now in %q is %q", player.Timezone, nowLocal))
		if nowLocal.Hour() == Midnight {
			midnightTimezones = append(midnightTimezones, player.Timezone)
		}
	}

	// Find players in timezones where now is midnight
	midnightPlayers := make(map[*datastore.Key]Player)
	// Note: because it doesn't exist a `IN` operator we must iterate over all
	// timezones where is midnight
	for _, timezone := range midnightTimezones {
		midnightPlayersQuery := datastore.NewQuery("Player").Filter("Timezone =", timezone)
		var players []Player
		playerKeys, err := midnightPlayersQuery.GetAll(c, &players)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		log.Debugf(c, fmt.Sprintf("Found %d players in timezone %q", len(players), timezone))
		for i, player := range players {
			midnightPlayers[playerKeys[i]] = player
		}
	}
	log.Debugf(c, fmt.Sprintf("Found %d players in timezones where is midnight", len(midnightPlayers)))

	achievers := make(map[*datastore.Key]Player)
	for playerKey, player := range midnightPlayers {
		// Check if the daily steps counter > DailyStepsGoal
		if player.DailySteps >= DailyStepsGoal {
			achievers[playerKey] = player
		}
	}
	log.Debugf(c, fmt.Sprintf("%d players achieved the goal", len(achievers)))

	// Reset the daily steps counter
	midnightPlayersKeys := make([]*datastore.Key, 0, len(midnightPlayers))
	updatedPlayers := make([]Player, 0, len(midnightPlayers))
	for playerKey, player := range midnightPlayers {
		midnightPlayersKeys = append(midnightPlayersKeys, playerKey)
		player.DailySteps = 0
		updatedPlayers = append(updatedPlayers, player)
	}
	if _, err := datastore.PutMulti(c, midnightPlayersKeys, updatedPlayers); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	log.Debugf(c, fmt.Sprintf("Updated %d players", len(midnightPlayersKeys)))

	// Find achievers' owners
	owners := make(map[*datastore.Key]int64)
	for playerKey, _ := range achievers {
		transactionsQuery := datastore.NewQuery("Transaction").Filter("PlayerId =", playerKey.IntID()).KeysOnly()
		var transactions []Player
		transactionKeys, err := transactionsQuery.GetAll(c, &transactions)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		log.Debugf(c, fmt.Sprintf("Found %d owners for player %d", len(transactionKeys), playerKey.IntID()))

		// For each owner include it in the map of owners and assign to it
		// `GoalBonus` or increase its bonus amount by `GoalBonus` if it's already
		// in the map
		for _, transactionKey := range transactionKeys {
			if bonusAmount, present := owners[transactionKey.Parent()]; present {
				owners[transactionKey.Parent()] = bonusAmount + GoalBonus
			} else {
				owners[transactionKey.Parent()] = GoalBonus
			}
		}
	}
	log.Debugf(c, fmt.Sprintf("Found %d unique owners", len(owners)))

	// Find owners
	ownersKeys := make([]*datastore.Key, 0, len(owners))
	for playerKey, _ := range owners {
		ownersKeys = append(ownersKeys, playerKey)
	}
	ownersPlayers := make([]Player, len(ownersKeys))
	if err := datastore.GetMulti(c, ownersKeys, ownersPlayers); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	log.Debugf(c, fmt.Sprintf("Found %d players", len(ownersPlayers)))

	// Update each owner player's balance by the bonus value in the map
	for i, playerKey := range ownersKeys {
		ownersPlayers[i].Balance = ownersPlayers[i].Balance + owners[playerKey]
	}
	if _, err := datastore.PutMulti(c, ownersKeys, ownersPlayers); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	log.Debugf(c, fmt.Sprintf("Updated %d players", len(ownersPlayers)))

	// Create a Sender to send the message
	client := urlfetch.Client(c)
	sender := &Sender{ApiKey: ApiKey, Http: client}

	// Send messages to owners that received a bonus
	go func() {
		for playerKey, bonusAmount := range owners {
			// Create message
			data := map[string]interface{}{"event": BonusEvent, "bonusAmount": bonusAmount}
			msg := NewMessage(data, fmt.Sprintf("/topics/player-%d", playerKey.IntID()))

			// Send the message and receive the response
			response, err := sender.SendNoRetry(msg)
			if err != nil {
				log.Errorf(c, fmt.Sprintf("Message delivery error: %q", err.Error()))
				return
			}
			log.Debugf(c, fmt.Sprintf("Message delivery response: %#v", response))
		}
	}()

	w.WriteHeader(http.StatusNoContent)
}
