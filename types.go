package hamusutaa

import (
	"net/http"
	"time"
)

const (
	InitialPrice     = 100
	InitialBalance   = 1000
	DailyStepsGoal   = 10000
	GoalBonus        = 100
	PingBonus        = 100
	MeasurementEvent = "com.yeahright.hamasutaa.MEASUREMENT"
	PingEvent        = "com.yeahright.hamasutaa.PING"
	BonusEvent       = "com.yeahright.hamasutaa.BONUS"
)

type Player struct {
	Name       string
	Color      string
	IsMoving   bool
	TotalSteps int64
	DailySteps int64
	Price      int64
	Balance    int64
	Timezone   string
	CreatedAt  time.Time
	UpdatedAt  time.Time
}

type PlayerResponse struct {
	Id         int64     `json:"id,string"`
	Name       string    `json:"name"`
	Color      string    `json:"color"`
	IsMoving   bool      `json:"isMoving"`
	TotalSteps int64     `json:"totalSteps"`
	DailySteps int64     `json:"dailySteps"`
	Price      int64     `json:"price"`
	Timezone   string    `json:"timezone"`
	CreatedAt  time.Time `json:"createdAt"`
	UpdatedAt  time.Time `json:"updatedAt"`
}

type CurrentPlayerResponse struct {
	Id         int64             `json:"id,string"`
	Name       string            `json:"name"`
	Color      string            `json:"color"`
	IsMoving   bool              `json:"isMoving"`
	TotalSteps int64             `json:"totalSteps"`
	DailySteps int64             `json:"dailySteps"`
	Price      int64             `json:"price"`
	Balance    int64             `json:"balance"`
	Timezone   string            `json:"timezone"`
	Players    []*PlayerResponse `json:"players"`
	CreatedAt  time.Time         `json:"createdAt"`
	UpdatedAt  time.Time         `json:"updatedAt"`
}

type PlayerRequest struct {
	Name     string `json:"name"`
	Color    string `json:"color"`
	Timezone string `json:"timezone"`
}

type Measurement struct {
	Steps     int64
	IsMoving  bool
	CreatedAt time.Time
}

type MeasurementRequest struct {
	Steps    int64 `json:"steps"`
	IsMoving bool  `json:"isMoving"`
}

type Token struct {
	PlayerId  int64
	CreatedAt time.Time
}

type TokenResponse struct {
	Token    int64 `json:"token,string"`
	PlayerId int64 `json:"playerId,string"`
}

type Transaction struct {
	// The player that has been bought
	PlayerId  int64
	Amount    int64
	CreatedAt time.Time
}

type Ping struct {
	// The player that sent the ping
	PlayerId int64
	// True if this ping has been awarded with a bonus
	BonusAwarded bool
	CreatedAt    time.Time
	UpdatedAt    time.Time
}

// The PlayerHandlerFunc type is an adapter to allow the use of
// ordinary functions as HTTP handlers.  If f is a function
// with the appropriate signature, PlayerHandlerFunc(f) is a
// Handler that calls f.
type PlayerHandlerFunc func(int64, Player, http.ResponseWriter, *http.Request)

// ServeHTTP calls f(id, p, w, r).
func (f PlayerHandlerFunc) ServeHTTP(id int64, p Player, w http.ResponseWriter, r *http.Request) {
	f(id, p, w, r)
}

// The AuthHandlerFunc type is an adapter to allow the use of
// ordinary functions as HTTP handlers.  If f is a function
// with the appropriate signature, AuthHandlerFunc(f) is a
// Handler that calls f.
type AuthHandlerFunc func(int64, Player, Token, Player, http.ResponseWriter, *http.Request)

// ServeHTTP calls f(id, p, t, w, r).
func (f AuthHandlerFunc) ServeHTTP(id int64, p Player, t Token, ap Player, w http.ResponseWriter, r *http.Request) {
	f(id, p, t, ap, w, r)
}
