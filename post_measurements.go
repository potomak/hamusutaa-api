package hamusutaa

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"google.golang.org/appengine"
	"google.golang.org/appengine/datastore"
	"google.golang.org/appengine/log"
	"google.golang.org/appengine/urlfetch"
)

// The maximum number of seconds the current player could wait before moving to
// give a bonus to the player who pinged
const PingTTL = time.Duration(10) * time.Minute

// POST /measurements
// Creates a new `measurement` record associated to the current player.
func handlePOSTMeasurements(_ int64, _ Player, token Token, currentPlayer Player, w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
	log.Debugf(c, fmt.Sprintf("POST Measurements player id: %d", token.PlayerId))

	// Create a Sender to send messages
	client := urlfetch.Client(c)
	sender := &Sender{ApiKey: ApiKey, Http: client}

	decoder := json.NewDecoder(r.Body)
	var measurementRequest MeasurementRequest
	err := decoder.Decode(&measurementRequest)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	log.Debugf(c, fmt.Sprintf("New measurement request: %#v", measurementRequest))

	measurement := &Measurement{
		Steps:     measurementRequest.Steps,
		IsMoving:  measurementRequest.IsMoving,
		CreatedAt: time.Now(),
	}
	log.Debugf(c, fmt.Sprintf("New measurement: %#v", measurement))

	playerKey := datastore.NewKey(c, "Player", "", token.PlayerId, nil)
	measurementKey := datastore.NewIncompleteKey(c, "Measurement", playerKey)
	_, err = datastore.Put(c, measurementKey, measurement)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Check if the player is moving in response to a ping
	if measurement.IsMoving {
		log.Debugf(c, "Player is moving, let's check for ping bonus...")

		var pings []Ping
		var pingKeys []*datastore.Key
		q := datastore.NewQuery("Ping").Ancestor(playerKey).Order("-CreatedAt").Limit(1)
		pingKeys, err := q.GetAll(c, &pings)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		log.Debugf(c, fmt.Sprintf("Pings found: %#v", pings))

		// Check if a ping has been found, if it's still fresh, and hasn't been
		// awarded yet
		if len(pings) > 0 && pings[0].CreatedAt.Add(PingTTL).After(time.Now()) && !pings[0].BonusAwarded {
			// Update ping, set as awarded
			ping := &Ping{
				PlayerId:     pings[0].PlayerId,
				BonusAwarded: true,
				CreatedAt:    pings[0].CreatedAt,
				UpdatedAt:    time.Now(),
			}
			log.Debugf(c, fmt.Sprintf("Updated ping: %#v", ping))

			_, err = datastore.Put(c, pingKeys[0], ping)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}

			var pinger Player
			pingerKey := datastore.NewKey(c, "Player", "", pings[0].PlayerId, nil)
			err = datastore.Get(c, pingerKey, &pinger)
			if err != nil && err != datastore.ErrNoSuchEntity {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			log.Debugf(c, fmt.Sprintf("Found the 'pinger': %q (%d)", pinger.Name, pingerKey.IntID()))

			updatedPinger := &Player{
				Name:       pinger.Name,
				Color:      pinger.Color,
				IsMoving:   pinger.IsMoving,
				TotalSteps: pinger.TotalSteps,
				DailySteps: pinger.DailySteps,
				Price:      pinger.Price,
				Balance:    pinger.Balance + PingBonus,
				Timezone:   pinger.Timezone,
				CreatedAt:  pinger.CreatedAt,
				UpdatedAt:  time.Now(),
			}
			log.Debugf(c, fmt.Sprintf("Updated pinger player: %#v", updatedPinger))

			_, err = datastore.Put(c, pingerKey, updatedPinger)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}

			data := map[string]interface{}{"event": BonusEvent, "bonusAmount": PingBonus}
			msg := NewMessage(data, fmt.Sprintf("/topics/player-%d", pingerKey.IntID()))

			// Send the message and receive the response
			go func() {
				response, err := sender.SendNoRetry(msg)
				if err != nil {
					log.Errorf(c, fmt.Sprintf("Message delivery error: %q", err.Error()))
					return
				}
				log.Debugf(c, fmt.Sprintf("Message delivery response: %#v", response))
			}()
		} else {
			log.Debugf(c, "Ping not found, not fresh enough, or already awarded")
		}
	}

	updatedPlayer := &Player{
		Name:       currentPlayer.Name,
		Color:      currentPlayer.Color,
		IsMoving:   measurement.IsMoving,
		TotalSteps: currentPlayer.TotalSteps + measurement.Steps,
		DailySteps: currentPlayer.DailySteps + measurement.Steps,
		Price:      currentPlayer.Price,
		Balance:    currentPlayer.Balance + measurement.Steps,
		Timezone:   currentPlayer.Timezone,
		CreatedAt:  currentPlayer.CreatedAt,
		UpdatedAt:  time.Now(),
	}
	log.Debugf(c, fmt.Sprintf("Updated player: %#v", updatedPlayer))

	_, err = datastore.Put(c, playerKey, updatedPlayer)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Send notification if player's status changed
	if currentPlayer.IsMoving != updatedPlayer.IsMoving {
		// Create message
		data := map[string]interface{}{"event": MeasurementEvent, "isMoving": updatedPlayer.IsMoving}
		msg := NewMessage(data, fmt.Sprintf("/topics/player-%d", token.PlayerId))

		go func() {
			// Send the message and receive the response
			response, err := sender.SendNoRetry(msg)
			if err != nil {
				log.Errorf(c, fmt.Sprintf("Message delivery error: %q", err.Error()))
				return
			}
			log.Debugf(c, fmt.Sprintf("Message delivery response: %#v", response))
		}()
	} else {
		log.Debugf(c, "Skipping notification because status didn't change")
	}

	w.WriteHeader(http.StatusNoContent)
}
