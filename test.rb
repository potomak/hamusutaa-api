#!/usr/bin/env ruby

require 'json'
require 'logger'
require 'net/http'
require 'test/unit'

include Test::Unit::Assertions

LOG = Logger.new(STDOUT)
#LOG.level = Logger::DEBUG
LOG.level = Logger::INFO

http = Net::HTTP.new('localhost', 8080)
#http = Net::HTTP.new('hashire-hamusutaa.appspot.com')

def get_player(http, player_id)
  request = Net::HTTP::Get.new("/players/#{player_id}")
  make_request("GET /players/#{player_id}", http, request)
end

def get_me(http, token)
  header = {'X-Auth-Token' => token}
  request = Net::HTTP::Get.new("/me", initheader = header)
  make_request('GET /me', http, request)
end

def post_players(http, name, color, timezone)
  header = {'Content-Type' => 'application/json'}
  request = Net::HTTP::Post.new('/players/', initheader = header)
  request.body = {name: name, color: color, timezone: timezone}.to_json
  status, post_player_response = make_request("POST /players/ (#{name})", http, request)
  assert_success(status)
  assert(post_player_response != nil)
  post_player_response
end

def make_request(label, http, request)
  LOG.debug '------------------------------------------'
  LOG.info label
  response = http.request(request)

  LOG.debug "Response: #{response.code}"
  if response.code.to_i >= 200 && response.code.to_i < 300
    if response.body
      parsed_response = JSON.parse(response.body)
      LOG.debug JSON.pretty_generate(parsed_response)
    end
  else
    LOG.debug "Error: #{response.body}"
  end
  LOG.debug '------------------------------------------'

  return response.code.to_i, parsed_response ? parsed_response : response.body
end

def assert_success(status)
  assert(status >= 200 && status < 300)
end

request = Net::HTTP::Get.new("/players/")
status, response = make_request('GET /players/', http, request)
assert_success(status)
players_number = response.size

request = Net::HTTP::Get.new("/players/?maxPrice=100")
status, response = make_request('GET /players/?maxPrice=100', http, request)
assert_success(status)
max_100_players_number = response.size

request = Net::HTTP::Get.new("/players/?maxPrice=asd")
status, response = make_request('GET /players/?maxPrice=asd', http, request)
assert_success(status)
assert(response.size == players_number)

post_player_response = post_players(http, 'Giovanni', '#00ccff', 'America/New_York')
token = post_player_response['token']
player_id = post_player_response['playerId']

status, response = get_player(http, player_id)
assert_success(status)
assert(response['id'] == player_id)
assert(response['name'] == 'Giovanni')
assert(response['color'] == '#00ccff')
assert(response['timezone'] == 'America/New_York')
assert(response['isMoving'] == false)
assert(response['totalSteps'] == 0)
assert(response['dailySteps'] == 0)
assert(response['price'] == 100)

status, response = get_me(http, token)
assert_success(status)
assert(response['id'] == player_id)
assert(response['name'] == 'Giovanni')
assert(response['color'] == '#00ccff')
assert(response['timezone'] == 'America/New_York')
assert(response['isMoving'] == false)
assert(response['totalSteps'] == 0)
assert(response['dailySteps'] == 0)
assert(response['price'] == 100)
assert(response['balance'] == 1000)
assert(response['players'] == [])

request = Net::HTTP::Get.new("/players/")
status, response = make_request('GET /players/', http, request)
assert_success(status)
assert(response.size == players_number + 1)

request = Net::HTTP::Get.new("/players/?maxPrice=100")
status, response = make_request('GET /players/?maxPrice=100', http, request)
assert_success(status)
assert(response.size == max_100_players_number + 1)

header = {'Content-Type' => 'application/json', 'X-Auth-Token' => token}
request = Net::HTTP::Put.new("/me", initheader = header)
request.body = {name: 'Giovanni', color: '#ffffff', timezone: 'America/New_York'}.to_json
status, _ = make_request('PUT /me', http, request)
assert_success(status)

status, response = get_me(http, token)
assert_success(status)
assert(response['name'] == 'Giovanni')
assert(response['color'] == '#ffffff')
assert(response['timezone'] == 'America/New_York')

header = {'Content-Type' => 'application/json', 'X-Auth-Token' => token}
request = Net::HTTP::Post.new("/measurements", initheader = header)
request.body = {steps: 12, isMoving: true}.to_json
status, _ = make_request('POST /measurements', http, request)
assert_success(status)

status, response = get_me(http, token)
assert_success(status)
assert(response['isMoving'] == true)
assert(response['totalSteps'] == 12)
assert(response['dailySteps'] == 12)
assert(response['balance'] == 1012)

header = {'Content-Type' => 'application/json', 'X-Auth-Token' => token}
request = Net::HTTP::Post.new("/measurements", initheader = header)
request.body = {steps: 2, isMoving: false}.to_json
status, _ = make_request('POST /measurements (isMoving false)', http, request)
assert_success(status)

status, response = get_me(http, token)
assert_success(status)
assert(response['isMoving'] == false)
assert(response['totalSteps'] == 14)
assert(response['dailySteps'] == 14)
assert(response['balance'] == 1014)

post_player_response = post_players(http, 'Giorgia', '#ffcc00', 'America/New_York')
token_2 = post_player_response['token']
player_2_id = post_player_response['playerId']

status, response = get_player(http, player_2_id)
assert_success(status)
assert(response['id'] == player_2_id)
assert(response['name'] == 'Giorgia')
assert(response['color'] == '#ffcc00')
assert(response['timezone'] == 'America/New_York')
assert(response['isMoving'] == false)
assert(response['totalSteps'] == 0)
assert(response['dailySteps'] == 0)
assert(response['price'] == 100)

request = Net::HTTP::Get.new("/players/")
status, response = make_request('GET /players/', http, request)
assert_success(status)
assert(response.size == players_number + 2)

request = Net::HTTP::Get.new("/players/?maxPrice=100")
status, response = make_request('GET /players/?maxPrice=100', http, request)
assert_success(status)
assert(response.size == max_100_players_number + 2)

header = {'X-Auth-Token' => token}
request = Net::HTTP::Post.new("/players/#{player_2_id}/ping", initheader = header)
status, response = make_request('POST /players/:id/ping (Giovanni -> Giorgia, it should fail)', http, request)
assert(status == 400)
assert(response =~ /You can't ping a player you don't own/)

header = {'X-Auth-Token' => token}
request = Net::HTTP::Post.new("/players/#{player_2_id}/buy", initheader = header)
status, _ = make_request('POST /players/:id/buy (Giovanni -> Giorgia)', http, request)
assert_success(status)

request = Net::HTTP::Get.new("/players/")
status, response = make_request('GET /players/', http, request)
assert_success(status)
assert(response.size == players_number + 2)

request = Net::HTTP::Get.new("/players/?maxPrice=100")
status, response = make_request('GET /players/?maxPrice=100', http, request)
assert_success(status)
assert(response.size == max_100_players_number + 1)

status, response = get_me(http, token)
assert_success(status)
assert(response['players'].size == 1)
assert(response['players'][0]['id'] == player_2_id)
assert(response['players'][0]['name'] == 'Giorgia')
assert(response['balance'] == 914)

status, response = get_player(http, player_2_id)
assert_success(status)
assert(response['price'] == 10100)

header = {'X-Auth-Token' => token}
request = Net::HTTP::Post.new("/players/#{player_2_id}/buy", initheader = header)
status, response = make_request('POST /players/:id/buy (Giovanni -> Giorgia, it should fail)', http, request)
assert(status == 400)
assert(response =~ /Insufficient funds/)

header = {'Content-Type' => 'application/json', 'X-Auth-Token' => token}
request = Net::HTTP::Post.new("/measurements", initheader = header)
request.body = {steps: 10000, isMoving: true}.to_json
status, _ = make_request('POST /measurements', http, request)
assert_success(status)

status, response = get_me(http, token)
assert_success(status)
assert(response['isMoving'] == true)
assert(response['totalSteps'] == 10014)
assert(response['dailySteps'] == 10014)
assert(response['balance'] == 10914)

header = {'X-Auth-Token' => token}
request = Net::HTTP::Post.new("/players/#{player_2_id}/buy", initheader = header)
status, response = make_request('POST /players/:id/buy (Giovanni -> Giorgia, it should fail)', http, request)
assert(status == 400)
assert(response =~ /You can't buy the same player twice/)

header = {'X-Auth-Token' => token}
request = Net::HTTP::Post.new("/players/#{player_2_id}/ping", initheader = header)
status, _ = make_request('POST /players/:id/ping (Giovanni -> Giorgia)', http, request)
assert_success(status)

header = {'X-Auth-Token' => token}
request = Net::HTTP::Post.new("/players/#{player_2_id}/ping", initheader = header)
status, response = make_request('POST /players/:id/ping (Giovanni -> Giorgia, it should fail)', http, request)
assert(status == 400)
assert(response =~ /You can't ping a player more than once/)

header = {'Content-Type' => 'application/json', 'X-Auth-Token' => token_2}
request = Net::HTTP::Post.new("/measurements", initheader = header)
request.body = {steps: 12, isMoving: true}.to_json
status, _ = make_request('POST /measurements (Giorgia)', http, request)
assert_success(status)

status, response = get_player(http, player_2_id)
assert_success(status)
assert(response['isMoving'] == true)
assert(response['totalSteps'] == 12)
assert(response['dailySteps'] == 12)

status, response = get_me(http, token)
assert_success(status)
assert(response['balance'] == 11014)

header = {'Content-Type' => 'application/json', 'X-Auth-Token' => token_2}
request = Net::HTTP::Post.new("/measurements", initheader = header)
request.body = {steps: 1, isMoving: false}.to_json
status, _ = make_request('POST /measurements (Giorgia)', http, request)
assert_success(status)

header = {'Content-Type' => 'application/json', 'X-Auth-Token' => token_2}
request = Net::HTTP::Post.new("/measurements", initheader = header)
request.body = {steps: 1, isMoving: true}.to_json
status, _ = make_request('POST /measurements (Giorgia)', http, request)
assert_success(status)

# It doesn't count the same ping twice
status, response = get_me(http, token)
assert_success(status)
assert(response['balance'] == 11014)

header = {'X-Auth-Token' => token}
request = Net::HTTP::Post.new("/players/#{player_2_id}/ping", initheader = header)
status, response = make_request('POST /players/:id/ping (Giovanni -> Giorgia, it should fail)', http, request)
assert(status == 400)
assert(response =~ /You can't ping a player that is active/)

header = {'Content-Type' => 'application/json', 'X-Auth-Token' => token_2}
request = Net::HTTP::Post.new("/measurements", initheader = header)
request.body = {steps: 2, isMoving: false}.to_json
status, _ = make_request('POST /measurements (Giorgia, isMoving false)', http, request)
assert_success(status)

status, response = get_player(http, player_2_id)
assert_success(status)
assert(response['isMoving'] == false)
assert(response['totalSteps'] == 16)
assert(response['dailySteps'] == 16)

header = {'X-Auth-Token' => token}
request = Net::HTTP::Post.new("/players/#{player_2_id}/ping", initheader = header)
status, _ = make_request('POST /players/:id/ping (Giovanni -> Giorgia)', http, request)
assert_success(status)

header = {'X-Auth-Token' => token}
request = Net::HTTP::Post.new("/players/#{player_2_id}/ping", initheader = header)
status, response = make_request('POST /players/:id/ping (Giovanni -> Giorgia, it should fail)', http, request)
assert(status == 400)
assert(response =~ /You can't ping a player more than once/)
