package hamusutaa

import (
	"fmt"
	"net/http"
	"time"

	"google.golang.org/appengine"
	"google.golang.org/appengine/datastore"
	"google.golang.org/appengine/log"
)

// POST /players/:id/buy
// Creates a `buy` record between the current player and the selected player.
func handlePOSTPlayersBuyWithId(playerId int64, player Player, token Token, currentPlayer Player, w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
	log.Debugf(c, fmt.Sprintf("Buy player id: %d", playerId))

	// Check that current player has sufficient funds to buy the selected player
	if currentPlayer.Balance < player.Price {
		log.Debugf(c, "Insufficient funds")
		log.Debugf(c, fmt.Sprintf("currentPlayer.Balance: %d", currentPlayer.Balance))
		log.Debugf(c, fmt.Sprintf("player.Price: %d", player.Price))
		http.Error(w, "Insufficient funds", http.StatusBadRequest)
		return
	}

	// Return an error if the current player already bought the selected player
	currentPlayerKey := datastore.NewKey(c, "Player", "", token.PlayerId, nil)
	q := datastore.NewQuery("Transaction").Ancestor(currentPlayerKey).Filter("PlayerId =", playerId)
	transactionsCount, err := q.Count(c)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if transactionsCount > 0 {
		http.Error(w, "You can't buy the same player twice", http.StatusBadRequest)
		return
	}

	// Create a new transaction
	transaction := &Transaction{
		PlayerId:  playerId,
		Amount:    player.Price,
		CreatedAt: time.Now(),
	}
	log.Debugf(c, fmt.Sprintf("New transaction: %#v", transaction))

	transactionKey := datastore.NewIncompleteKey(c, "Transaction", currentPlayerKey)
	_, err = datastore.Put(c, transactionKey, transaction)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Update the current player's balance
	updatedCurrentPlayer := &Player{
		Name:       currentPlayer.Name,
		Color:      currentPlayer.Color,
		IsMoving:   currentPlayer.IsMoving,
		TotalSteps: currentPlayer.TotalSteps,
		DailySteps: currentPlayer.DailySteps,
		Price:      currentPlayer.Price,
		Balance:    currentPlayer.Balance - player.Price,
		Timezone:   currentPlayer.Timezone,
		CreatedAt:  currentPlayer.CreatedAt,
		UpdatedAt:  time.Now(),
	}
	log.Debugf(c, fmt.Sprintf("Updated currentPlayer: %#v", updatedCurrentPlayer))

	_, err = datastore.Put(c, currentPlayerKey, updatedCurrentPlayer)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Increase selected player's price by `DailyStepsGoal` $, that's the
	// number of steps in a day needed to get a bonus
	updatedPlayer := &Player{
		Name:       player.Name,
		Color:      player.Color,
		IsMoving:   player.IsMoving,
		TotalSteps: player.TotalSteps,
		DailySteps: player.DailySteps,
		Price:      player.Price + DailyStepsGoal,
		Balance:    player.Balance,
		Timezone:   player.Timezone,
		CreatedAt:  player.CreatedAt,
		UpdatedAt:  time.Now(),
	}
	log.Debugf(c, fmt.Sprintf("Updated player: %#v", updatedPlayer))

	playerKey := datastore.NewKey(c, "Player", "", playerId, nil)
	_, err = datastore.Put(c, playerKey, updatedPlayer)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusNoContent)
}
