package hamusutaa

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"golang.org/x/net/context"

	"google.golang.org/appengine/datastore"
	"google.golang.org/appengine/log"
)

// POST /players/
// Creates a new player.
func handlePOSTPlayers(c context.Context, w http.ResponseWriter, r *http.Request) {
	decoder := json.NewDecoder(r.Body)
	var playerRequest PlayerRequest
	err := decoder.Decode(&playerRequest)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	log.Debugf(c, fmt.Sprintf("New player request: %#v", playerRequest))

	player := &Player{
		Name:       playerRequest.Name,
		Color:      playerRequest.Color,
		IsMoving:   false,
		TotalSteps: 0,
		DailySteps: 0,
		Price:      InitialPrice,
		Balance:    InitialBalance,
		Timezone:   playerRequest.Timezone,
		CreatedAt:  time.Now(),
		UpdatedAt:  time.Now(),
	}
	log.Debugf(c, fmt.Sprintf("New player: %#v", player))

	playerKey := datastore.NewIncompleteKey(c, "Player", nil)
	playerKey, err = datastore.Put(c, playerKey, player)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	log.Debugf(c, fmt.Sprintf("playerKey: %#v", playerKey))

	token := &Token{
		PlayerId:  playerKey.IntID(),
		CreatedAt: time.Now(),
	}
	tokenKey := datastore.NewIncompleteKey(c, "Token", nil)
	tokenKey, err = datastore.Put(c, tokenKey, token)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	log.Debugf(c, fmt.Sprintf("tokenKey: %#v", tokenKey))
	log.Debugf(c, fmt.Sprintf("token: %#v", token))

	tokenResponse := &TokenResponse{
		Token:    tokenKey.IntID(),
		PlayerId: playerKey.IntID(),
	}
	js, err := json.Marshal(tokenResponse)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(js)
}
